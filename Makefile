CXX=g++

OBJDIR     = obj
TARGETDIR  = .
TARGET     = $(TARGETDIR)/cosmos
INCLUDES  += -I./include -I./libraries/include
CPPFLAGS  += -MMD -MP $(DEFINES) $(INCLUDES)
CFLAGS    += $(CPPFLAGS) $(ARCH) -Wall -std=c++11 -g -O3
CXXFLAGS  += $(CFLAGS)
LIBS      += ./libraries/*.a -lGL -lGLEW -lSOIL \
	     -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor -lrt -ldl -lm -pthread

OBJECTS := \
	$(OBJDIR)/cosmos.o	    \
	$(OBJDIR)/callbacks.o	    \
	$(OBJDIR)/camera.o	    \
	$(OBJDIR)/sphere.o	    \
	$(OBJDIR)/renderer.o	    \
	$(OBJDIR)/deep_space.o	    \
	$(OBJDIR)/celestial_body.o  \
	$(OBJDIR)/black_hole.o	    \
	$(OBJDIR)/stars.o

DEPS 	:= $(OBJECTS:.o=.d)

all: $(OBJDIR) $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(OBJECTS) $(LIBS) -o $(TARGET)

-include $(DEPS)

clean:
	rm -f $(OBJECTS)
	rm -f $(OBJECTS:%.o=%.d)
	rm -f $(TARGET);

$(OBJDIR):
	mkdir $(subst /,\\,$(OBJDIR))

$(OBJDIR)/cosmos.o: source/cosmos.cpp
	@echo $(notdir $<)
	$(CXX) $(CXXFLAGS) -o "$@" -MF $(@:%.o=%.d) -c "$<"

$(OBJDIR)/callbacks.o: source/callbacks.cpp
	@echo $(notdir $<)
	$(CXX) $(CXXFLAGS) -o "$@" -MF $(@:%.o=%.d) -c "$<"

$(OBJDIR)/camera.o: source/rendering/camera.cpp
	@echo $(notdir $<)
	$(CXX) $(CXXFLAGS) -o "$@" -MF $(@:%.o=%.d) -c "$<"

$(OBJDIR)/sphere.o: source/sphere.cpp
	@echo $(notdir $<)
	$(CXX) $(CXXFLAGS) -o "$@" -MF $(@:%.o=%.d) -c "$<"

$(OBJDIR)/renderer.o: source/rendering/renderer.cpp
	@echo $(notdir $<)
	$(CXX) $(CXXFLAGS) -o "$@" -MF $(@:%.o=%.d) -c "$<"

$(OBJDIR)/deep_space.o: source/rendering/background/deep_space.cpp
	@echo $(notdir $<)
	$(CXX) $(CXXFLAGS) -o "$@" -MF $(@:%.o=%.d) -c "$<"

$(OBJDIR)/celestial_body.o: source/rendering/foreground/celestial_body.cpp
	@echo $(notdir $<)
	$(CXX) $(CXXFLAGS) -o "$@" -MF $(@:%.o=%.d) -c "$<"

$(OBJDIR)/black_hole.o: source/rendering/foreground/black_hole.cpp
	@echo $(notdir $<)
	$(CXX) $(CXXFLAGS) -o "$@" -MF $(@:%.o=%.d) -c "$<"

$(OBJDIR)/stars.o: source/rendering/foreground/stars.cpp
	@echo $(notdir $<)
	$(CXX) $(CXXFLAGS) -o "$@" -MF $(@:%.o=%.d) -c "$<"
