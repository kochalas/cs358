// sphere.hpp: an easy way to produce detailed Spheres
// @authors Angelos Sfakianakis, Konstantinos Chalas, Alex Shevtsov


#define SPHERE_RESOLUTION 3


#ifndef __SPHERE__
#define __SPHERE__

#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <unordered_map>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>


/**
 * Produces new Spheres to be drawn using OpenGL.
 */
class SphereFactory {
    struct Triangle {
        GLuint a, b, c;

        static void prepare(Triangle &triangle, GLuint a, GLuint b, GLuint c) {
            triangle.a = a;
            triangle.b = b;
            triangle.c = c;
        }
    };


private:

    glm::vec3 *points;
    Triangle *triangles;
    unsigned int num_points, num_triangles;


    /**
     * Creates a regular Icosahedron.
     */
    void initialize(void);

    /**
     *
     * @param dictionary Hash Table that contains all the Points of the Model.
     * @param a The first Point of a Line Segment.
     * @param b The second Point of a Line Segment.
     *
     * @return The index of a middle Point.
     */
    GLuint fetch_point(std::unordered_map<unsigned int, unsigned int> &dictionary,
                      unsigned int a, unsigned int b);

    /**
     * Refines the Icosahedral by splitting each Triangular Face into four.
     */
    void refine(void);


    /**
     * Inflates the Sphere.
     *
     * @radius THe new radius of the Sphere.
     */
    void inflate(float radius);


public:

    /**
     * Create a new Sphere Factory. This is used to generate multiple Spheres,
     * without recalculating all the Points and Normals.
     *
     * @param resolution The number of recursive calls to the refining function.
     */
    SphereFactory(unsigned int resolution);

    /**
     * Destroys the Sphere Factory and releases any Memory used for the
     * pre-calculated Points.
     */
    ~SphereFactory(void) {
        delete[] points;
        delete[] triangles;
    }


    /**
     * Creates a new Sphere. Note that the Indices must be in position 0 while
     * the Points must be in position 1 of the GLSL Program.
     *
     * @param radius The radius of the new Sphere.
     * @param indices A handle for the Indices of the Sphere in the GPU.
     * @param points A handle for the Points of the Sphere in the GPU.
     *
     * @return The number of Points to draw.
     */
    unsigned int create(float radius, GLuint &indices, GLuint &points);
};


// a factory that generates new Spheres
extern SphereFactory factory;

#endif
