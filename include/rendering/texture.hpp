// texture.hpp: a helper class to load Textures
// @authors Konstantinos Chalas, Alex Shevtsov, Angelos Sfakianakis


#define TEXTURES_DIRECTORY "textures/"


#ifndef __TEXTURE__
#define __TEXTURE__

#include <cstdlib>
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>

#include <SOIL/SOIL.h>


#define texture_bind(active, target, texture) { \
    glActiveTexture(active);                    \
    glBindTexture(target, texture);             \
}


class Texture {
    GLenum target;
    GLuint texture;

public:

    /**
     * Loads a new Texture from an Image File.
     *
     * @param file The path of the File that contains the Image.
     * @param target The OpenGL Texture Target.
     */
    Texture(std::string file, GLenum target) : target(target) {
        int channels = 4;

        // load the Image from File
        int width, height;
        unsigned char *image = SOIL_load_image(
                (TEXTURES_DIRECTORY + file).c_str(),
                &width, &height,
                &channels, SOIL_LOAD_RGBA
        );
        if (image == NULL) throw -1;

        // create a new Texture on the GPU
        glGenTextures(1, &texture);
        glBindTexture(target, texture);
        glTexImage2D(target, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
        glTexParameterf(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(target, 0);

        SOIL_free_image_data(image); /* cleanup */
    };

    /**
     * Releases any Resources used by the Texture.
     */
    ~Texture(void) {
        glDeleteTextures(1, &texture);
    }


    /**
     * Binds Memory on the GPU and sends our Texture.
     *
     * @active The active Texture in OpenGL.
     */
    void bind(GLenum active) {
        texture_bind(active, target, texture);
    }
};

#endif
