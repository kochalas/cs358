// deep_space.hpp: drawable object used as the Background of the Cosmos
// @author Angelos Sfakianakis, Alex Shevtsov, Konstantinos Chalas


#define SPACE_RADIUS 200.0f


#ifndef __DEEP_SPACE__
#define __DEEP_SPACE__

#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

#include <sphere.hpp>

#include <rendering/drawable.hpp>


/**
 * Represents the Deep Space of the Cosmos.
 */
class DeepSpace : public Drawable {
    GLuint vao;
    GLuint indices, points; /* 0: indices, 1: points */
    unsigned int count;

    GLint gl_color;
    glm::vec3 position, color;


public:
    /**
     *  Create the Deep Space of the Cosmos.
     *
     * @param position The original Position of the Sphere.
     * @param color The color of the background.
     * @param program The OpenGL Program that will render the Sphere.
     */
    DeepSpace(glm::vec3 position, glm::vec3 color, GLuint program);


    /**
     * Gets the Color of the Stars in Deep Space.
     */
    glm::vec3 getColor(void) const {
        return color;
    }

    /**
     * Sets the Color of the Stars in Deep Space.
     */
    void setColor(glm::vec3 color) {
        this->color = color;
    }


    /**
     * Draws the object.
     */
    void draw(void) const override;

    /**
     * Updates the state of the twinkling Stars and the position of the
     * background Nebula.
     *
     * @dt The time elapsed since the previous call.
     *
     * @return Always True, unless all Hell breaks loose. I exists only for
     * compatibility reasons.
     */
    bool update(float dt) override;
};

#endif
