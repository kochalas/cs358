// camera.hpp: a simple moving Camera
// @authors Alex Shevtsov, Angelos Sfakianakis, Konstantinos Chalas


#define NEAR_CLIP   0.1f
#define FAR_CLIP    400.0f


#ifndef __CAMERA__
#define __CAMERA__

#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>


// An abstract camera class that processes input and calculates the
// corresponding Euler Angles, Vectors and Matrices for use in OpenGL
class Camera {
    // Camera Attributes
    glm::vec3 position;
    glm::vec3 direction;

    glm::mat4 view;
    glm::mat4 eye;
    float zoom;

    // Camera options
    float aspectRatio;

    //movement true or false;
    bool should_move;

public:

    // Constructor with vectors
    Camera(float aspect_ratio, glm::vec3 position, glm::vec3 direction);


    // Returns the view matrix calculated using Euler Angles and the LookAt Matrix
    glm::mat4 GetViewMatrix();

    // Returns the view matrix calculated using Euler Angles and the LookAt Matrix
    glm::mat4 GetProjectionMatrix();

    glm::vec3 GetDirectionVector() {
        return this->direction;
    }

    glm::vec3 GetCameraVector() {
        return this->position;
    }

    // Set aspect ration function
    void SetAspectRatio(float aspect_ratio) {
        this->aspectRatio = aspect_ratio;
        this->updateCameraVectors();
    }

    // Stop or Start camera mouse move
    void ChangeMouseMove() {
        this->should_move = !this->should_move;
    }

    void ChangeZoom(float change){
        if((this->zoom >= 30.0f && change < 0.0f) || (this->zoom <= 75.0f && change > 0.0f))
            this->zoom += change;
    }
    // Processes input received from any keyboard-like input system. Accepts
    // input parameter in the form of camera defined ENUM (to abstract it
    // from windowing systems)
    void ProcessKeyboard(glm::vec3 camera);

    // Processes input received from a mouse input system. Expects the
    // offset value in both the x and y direction.
    void ProcessMouseMovement(glm::vec3 direction);

    // Processes input received from a mouse scroll-wheel event. Only
    // requires input on the vertical wheel-axis
    void ProcessMouseScroll(GLfloat yoffset);

private:
    // Calculates the front vector from the Camera's (updated) Euler Angles
    void updateCameraVectors();
};

#endif
