// renderer.hpp: renders a batch of Drawables using the same OpenGL program
// @authors Alex Shevtsov, Angelos Sfakianakis, Konstantinos Chalas


#define LOG_SIZE            1024
#define SHADERS_DIRECTORY   "shaders/"


#ifndef __RENDERER__
#define __RENDERER__

#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <list>
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

#include "drawable.hpp"


/**
 * A generic mechanism to render batches of similar Objects.
 */
class Renderer {
    std::list<Drawable *> drawables;

    GLuint program;


public:

    /**
     * Creates a new Renderer. It might throw an Exception.
     *
     * @param vertex_shader The file name of the Vertex Shader. (required)
     * @param fragment_shader The file name of the Fragment Shader. (required)
     * @param geometry_shader The file name of the Vertex Shader. (optional)
     */
    Renderer(const std::string &vertex_shader,
             const std::string &fragment_shader,
             const std::string &geometry_shader = "");

    /**
     * Releases any resources used by the Renderer.
     */
    virtual ~Renderer(void) {
        glDeleteProgram(program);
    }


    /**
     * Adds a new Drawable to be rendered.
     *
     * @param drawable A pointer to a Drawable Object. It must be located in
     * Dynamic Memory. Only the Renderer will delete it.
     */
    void addObject(Drawable *drawable) {
        drawables.push_back(drawable);
    }

    /**
     * Gets a read-only List of the drawable Objects associated with this Renderer.
     */
    const std::list<Drawable *> *getObjects(void) const {
        return &drawables;
    }

    /**
     * Gets the OpenGL Program used for Rendering.
     * @return
     */
    GLuint getProgram(void) {
        return program;
    }


    /**
     * Renders all the objects that belong to this Renderer.
     *
     * @dt The time elapsed since the previous call.
     */
    virtual void render(float dt);
};

#endif
