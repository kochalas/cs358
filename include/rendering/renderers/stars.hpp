// stars.hpp: renderer for the Star Cluster
// @authors Konstantinos Chalas, Angelos Sfakianakis, Alex Shevtsov


#ifndef __RENDERERS_STARS__
#define __RENDERERS_STARS__

#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <rendering/renderer.hpp>


/**
 * A Renderer for the Star Cluster.
 */
class StarsR : public Renderer {
    GLint ViewProjection;
    glm::vec3 color;


public:

    /**
     * Constructs a new Renderer for the Star Cluster. It might throw an Exception.
     *
     * @param vertex_shader The file name of the Vertex Shader. (required)
     * @param fragment_shader The file name of the Fragment Shader. (required)
     * @param geometry_shader The file name of the Vertex Shader. (required)
     */
    StarsR(const std::string &vertex_shader,
               const std::string &fragment_shader,
               const std::string &geometry_shader)
            : Renderer(vertex_shader, fragment_shader, geometry_shader) {
        // query the Uniform Locations within the Program
        ViewProjection = glGetUniformLocation(Renderer::getProgram(), "ViewProjection");

        // ensure that all Queries were successful
        if (ViewProjection < 0) /* error */
            throw -1;
    }


    /**
     * Sets the Transformation Matrices for the Renderer.
     *
     * @param ViewProjection The View-Projection Matrix.
     */
    void setTransformations(glm::mat4 &ViewProjection) {
        glUseProgram(Renderer::getProgram());
        glUniformMatrix4fv(this->ViewProjection, 1, GL_FALSE, glm::value_ptr(ViewProjection));
        glUseProgram(0);
    }


    /**
     * Renders all the objects that belong to this Renderer and reports some
     * Statistics to the user.
     *
     * @dt The time elapsed since the previous call.
     */
    void render(float dt) override {
        static unsigned int counter = 0;

        if (++counter == 100) /* print statistics */ {
            std::cout << "stars: " << Renderer::getObjects()->size() << std::endl;
            counter = 0;
        }

        Renderer::render(dt);
    }
};

#endif
