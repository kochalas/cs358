// glow.hpp: renders the Scene after applying a Glow Effect
// @authors Angelos Sfakianakis, ALex Shevtsov, Konstantinos Chalas


#ifndef __RENDERERS_GLOW__
#define __RENDERERS_GLOW__

#include <iostream>

#include <GL/glew.h>
#include <GL/gl.h>

#include <rendering/renderer.hpp>


static const GLfloat positions[] = {
    -1.0f, 1.0f,
    -1.0f, -1.0f,
    1.0f, -1.0f,
    -1.0f,  1.0f,
    1.0f, -1.0f,
    1.0f,  1.0f
};
static const GLfloat texture[] = {
    0.0f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 0.0f,
    1.0f, 1.0f
};


/**
 * A Renderer for the Glow Effect.
 */
class GlowR : public Renderer {
    GLuint vao, vbo;


public:

    /**
     * Constructs a new Renderer for the Glow Effect. It might throw an Exception.
     * Note that this Renderer will render the whole Screen using Textures from
     * the internal Framebuffer as input.
     *
     * @param vertex_shader The file name of the Vertex Shader. (required)
     * @param fragment_shader The file name of the Fragment Shader. (required)
     * @param window_width The width of the rendering Window.
     * @param window_height The height of the rendering Window.
     */
    GlowR(const std::string &vertex_shader, const std::string &fragment_shader,
          float window_width, float window_height)
            : Renderer(vertex_shader, fragment_shader, "") {
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glUseProgram(Renderer::getProgram());

        // bind the Vertex Buffer Object and send the Data
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(positions) + sizeof(texture), NULL, GL_STATIC_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(positions), positions);
        glBufferSubData(GL_ARRAY_BUFFER, sizeof(positions), sizeof(texture), texture);

        // specify the Pointers to the Data
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *) sizeof(positions));

        // send the Uniforms to the Shaders
        glUniform1i(glGetUniformLocation(Renderer::getProgram(), "original"), 0);
        glUniform1i(glGetUniformLocation(Renderer::getProgram(), "highlights"), 1);
        glUniform1f(glGetUniformLocation(Renderer::getProgram(), "resolutionX"), window_width);
        glUniform1f(glGetUniformLocation(Renderer::getProgram(), "resolutionY"), window_height);

        glUseProgram(0);
        glBindVertexArray(0);
    }


    /**
     * Renders all the objects that belong to this Renderer and reports some
     * Statistics to the user.
     *
     * @dt Not used. Only for compatibility purposes.
     */
    void render(float dt) override {
        glUseProgram(Renderer::getProgram());
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
        glUseProgram(0);
    }
};

#endif
