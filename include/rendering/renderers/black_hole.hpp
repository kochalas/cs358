// black_hole.hpp: renderer for the Black Hole
// @authors Angelos Sfakianakis, ALex Shevtsov, Konstantinos Chalas


#ifndef __RENDERERS_BLACKHOLE__
#define __RENDERERS_BLACKHOLE__

#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <iostream>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <rendering/renderer.hpp>


/**
 * A Renderer for the Black Holes.
 */
class BlackHoleR : public Renderer {
    GLint View, Projection;
    GLint time;


public:

    /**
     * Constructs a new Renderer for Black Holes. It might throw an Exception.
     * Note that Black Holes use Textures for Noise generation.
     *
     * @param vertex_shader The file name of the Vertex Shader. (required)
     * @param fragment_shader The file name of the Fragment Shader. (required)
     * @param geometry_shader The file name of the Vertex Shader. (optional)
     */
    BlackHoleR(const std::string &vertex_shader,
               const std::string &fragment_shader,
               const std::string &geometry_shader = "")
            : Renderer(vertex_shader, fragment_shader, geometry_shader) {
        // query the Uniform Locations within the Program
        View = glGetUniformLocation(Renderer::getProgram(), "View");
        Projection = glGetUniformLocation(Renderer::getProgram(), "Projection");
        time = glGetUniformLocation(Renderer::getProgram(), "time");

        // ensure that all Queries were successful
        if (View < 0 || Projection < 0 || time < 0) /* error */
            throw -1;
    }


    /**
     * Sets the Transformation Matrices for the Renderer.
     *
     * @param View The View Matrix.
     * @param Projection The Projection Matrix.
     */
    void setTransformations(glm::mat4 &View, glm::mat4 &Projection) {
        glUseProgram(Renderer::getProgram());
        glUniformMatrix4fv(this->View, 1, GL_FALSE, glm::value_ptr(View));
        glUniformMatrix4fv(this->Projection, 1, GL_FALSE, glm::value_ptr(Projection));
        glUseProgram(0);
    }

    /**
     * Sets the Time variable for the Renderer.
     *
     * @param time The real Time, as reported by the System.
     */
    void setTime(float time) {
        glUseProgram(Renderer::getProgram());
        glUniform1f(this->time, time);
        glUseProgram(0);
    }


    /**
     * Renders all the objects that belong to this Renderer and reports some
     * Statistics to the user.
     *
     * @dt The time elapsed since the previous call.
     */
    void render(float dt) override {
        static unsigned int counter = 0;

        if (++counter == 100) /* print statistics */ {
            std::cout << "black holes: " << Renderer::getObjects()->size() << std::endl;
            counter = 0;
        }

        Renderer::render(dt);
    }
};

#endif
