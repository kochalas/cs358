// deep_space.hpp: renderer for Deep Space
// @authors Konstantinos Chalas, Angelos Sfakianakis, Alex Shevtsov


#ifndef __RENDERERS_DEEPSPACE__
#define __RENDERERS_DEEPSPACE__

#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <rendering/renderer.hpp>


/**
 * A Renderer for the Deep Space.
 */
class DeepSpaceR : public Renderer {
    GLint ModelViewProjection;
    GLint time;


public:

    /**
     * Constructs a new Renderer for the Deep Space. It might throw an Exception.
     *
     * @param vertex_shader The file name of the Vertex Shader. (required)
     * @param fragment_shader The file name of the Fragment Shader. (required)
     * @param geometry_shader The file name of the Vertex Shader. (optional)
     */
    DeepSpaceR(const std::string& vertex_shader,
        const std::string& fragment_shader,
        const std::string& geometry_shader = "")
        : Renderer(vertex_shader, fragment_shader, geometry_shader) {
        // query the Uniform Locations within the Program
        ModelViewProjection = glGetUniformLocation(Renderer::getProgram(), "ModelViewProjection");
        time = glGetUniformLocation(Renderer::getProgram(), "time");

        // ensure that all Queries were successful
        if (ModelViewProjection < 0 || time < 0) /* error */
            throw -1;
    }


    /**
     * Sets the Transformation Matrices for the Renderer.
     *
     * @param View The View-Projection Matrix.
     */
    void setTransformations(glm::mat4 &ViewProjection) {
        glUseProgram(Renderer::getProgram());

        glUniformMatrix4fv(this->ModelViewProjection, 1, GL_FALSE, glm::value_ptr(ViewProjection));

        glUseProgram(0);
    }

    /**
     * Sets the Time variable for the Renderer.
     *
     * @param time The real Time, as reported by the System.
     */
    void setTime(float time) {
        glUseProgram(Renderer::getProgram());
        glUniform1f(this->time, time);
        glUseProgram(0);
    }
};

#endif
