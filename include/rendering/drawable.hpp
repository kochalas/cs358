// drawable.hpp: an interface for all the Objects that can be drawn
// @authors Konstantinos Chalas, Angelos Sfakianakis, Alex Shevtsov


#define THRESHOLD_DISTANCE  200.0f
#define THRESHOLD_PROXIMITY 0.1f


#ifndef __DRAWABLE__
#define __DRAWABLE__

/**
 * An interface for all Drawable Objects.
 */
class Drawable {
public:

    virtual ~Drawable(void) {}

    /**
     * Updates the state of the object.
     *
     * @dt The time elapsed since the previous call.
     *
     * @return True if the object is still valid, False if it has expired and
     * shall be removed.
    */
    virtual bool update(float dt) = 0;

    /**
     * Draws the object.
     */
    virtual void draw(void) const = 0;
};

#endif
