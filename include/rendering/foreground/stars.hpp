// stars.hpp: drawable object representing a Cluster of Stars
// @author Konstantinos Chalas, Angelos Sfakianakis, Alex Shevtsov


#define PREV_POSITIONS  30


#ifndef __STARS__
#define __STARS__

#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS

#include <vector>

#include <GL/gl.h>
#include <glm/glm.hpp>

#include <rendering/foreground/celestial_body.hpp>


/**
 * An object representing a cluster of stars
 */
class Stars : public CelestialBody {
    GLuint vao, vbo;
    GLint gl_position_history, gl_color;
    unsigned int cluster_size;

    glm::vec3 cluster_history[PREV_POSITIONS];
    glm::vec3 *star_positions;
    glm::vec3 color;


    /**
     * Create the position for each individual Star in the Cluster.
     */
    void createStarPositions(void);


public:
    /**
     * Creates a new Cluster of Stars.
     *
     * @param cluster_history An array for the last Position.
     * @param color The color of the Cluster.
     */
    Stars(glm::vec3 position, glm::vec3 velocity, float mass, unsigned int cluster_size, glm::vec3 color,
          const std::list<CelestialBody *> *objects[], GLuint program);


    /**
     * Gets the Color of the Stars.
     */
    glm::vec3 getColor(void) const {
        return color;
    }

    /**
     * Sets the Color of the Stars.
     */
    void setColor(glm::vec3 color) {
        this->color = color;
    }


    /**
     * Disable the function, as Stars cannot absorb any other Celestial Body.
     */
    void absorbMass(CelestialBody& victim) override {}


    /**
     * Draws the object.
     */
    void draw(void) const override;

    /**
     * Updates the state of the Star Cluster, according to the new position
     *
     * @time The time elapsed since the previous call.
     *
     * @return True if the object is still valid, False if it has expired and
     * shall be removed.
     */
    bool update(float dt) override;
};

#endif
