// black_hole.hpp: drawable object representing a Black Hole
// @author Angelos Sfakianakis, Alex Shevtsov, Konstantinos Chalas


#define DENSITY 4.0f


#ifndef __BLACK_HOLE__
#define __BLACK_HOLE__

#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

#include <sphere.hpp>
#include <rendering/foreground/celestial_body.hpp>


/**
 * Represents a Black Hole.
 */
class BlackHole : public CelestialBody {
    GLuint vao;
    GLuint indices, points; /* 0: indices, 1: points */
    unsigned int count;

    GLint gl_translate, gl_color, gl_radius;
    glm::vec3 color;


public:

    /**
     *  Creates a new Black Hole.
     *
     * @param position The original Position of the Black Hole.
     * @param velocity The original Velocity of the Black Hole.
     * @param mass The mass of the Black Hole.
     * @param color The color of the Event Horizon.
     * @param objects An array of pointers to Lists that contain all the other
     * Celestial Bodies in the Scene.
     * @param program THe OpenGL Program that will render the Black Hole.
     */
    BlackHole(glm::vec3 position, glm::vec3 velocity, float mass, glm::vec3 color,
              const std::list<CelestialBody *> *objects[], GLuint program);


    /**
     * Gets the Color of the Event Horizon.
     */
    glm::vec3 getColor(void) const {
        return color;
    }

    /**
     * Sets the Color of the Event Horizon.
     */
    void setColor(glm::vec3 color) {
        this->color = color;
    }


    /**
     * Allow for Color mixing.
     */
    void absorbMass(CelestialBody& victim) override {
        BlackHole *_victim = dynamic_cast<BlackHole *>(&victim);
        if (_victim != NULL) /* it is a black hole */ {
            this->color = getMass() * color + _victim->getMass() * _victim->color;
            this->color /= getMass() + _victim->getMass();
        }

        CelestialBody::absorbMass(victim);
    }


    /**
     * Draws the object.
     */
    void draw(void) const override;
};

#endif
