// celestial_body.hpp: represents any possible Celestial Body in the scene
// @authors Konstantinos Chalas, Alex Shevtsov, Angelos Sfakianakis


#ifndef __CELESTIAL_BODY__
#define __CELESTIAL_BODY__

#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <list>
#include <cassert>

#include <glm/glm.hpp>

#include <rendering/drawable.hpp>


/**
 * A generic class for all Celestial Bodies.
 */
class CelestialBody : public Drawable {
    const std::list<CelestialBody *> **objects; /* NULL-terminated */

    glm::vec3 velocity;
    float mass; /* if mass is 0, the object has expired */


protected:

    glm::vec3 position;


    /**
     * Creates a new Celestial Body.
     *
     * @param position The original Position of the object.
     * @param velocity Th original Velocity of the object.
     * @param mass The mass of the object.
     * @param objects An array of pointers to Lists that contain all the other
     * Celestial Bodies in the Scene.
     */
    CelestialBody(glm::vec3 position, glm::vec3 velocity, float mass,
                  const std::list<CelestialBody *> *objects[])
            : objects(objects), velocity(velocity), mass(mass), position(position) {}


public:

    /**
     * Gets the object's current Position.
     */
    glm::vec3 getPosition(void) const {
        return position;
    }

    /**
     * Gets the object's Velocity.
     */
    glm::vec3 getVelocity(void) const {
        return velocity;
    }

    /**
     * Gets the object's Mass.
     */
    float getMass(void) const {
        return mass;
    }


    /**
     * Sets the object's Position.
     */
    void setPosition(glm::vec3 position) {
        this->position = position;
    }

    /**
     * Set the object's Velocity.
     */
    void setVelocity(glm::vec3 velocity) {
        this->velocity = velocity;
    }

    /**
     * Set the object's Mass.
     */
    void setMass(float mass) {
        this->mass = mass;
    }


    /**
     * Absorbs the mass of another object. Different objects might act in a
     * completely different way.
     *
     * @param victim The object to be absorbed. It should be destroyed afterwards,
     * as its state might be inconsistent.
     */
    virtual void absorbMass(CelestialBody &victim) {
        assert(this->mass >= 0.0f);
        if (this->mass == 0.0f) /* object has already expired, ignore */
            return;

        // merge the Objects using the formula for Conservation of Momentum
        this->velocity = (velocity * mass + victim.velocity * victim.mass);
        this->mass += victim.mass;
        this->velocity /= mass;

        victim.mass = 0.0f; /* expired */
    }


    /**
     * Updates the state of the Celestial Body, according to the Law of
     * Universal Gravitation.
     *
     * @dt The time elapsed since the previous call.
     *
     * @return True if the object is still valid, False if it has expired and
     * shall be removed.
     */
    bool update(float dt) override;

    /**
     * The distance of the Celestial Body from the Axes' Origin.
     */
    float distance(void) const {
        return glm::length(position);
    }
};

#endif
