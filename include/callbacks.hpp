// callbacks.hpp: callback functions for GLFW
// @authors Alex Shevtsov, Angelos Sfakianakis, Konstantinos Chalas


#include <GLFW/glfw3.h>


void MouseButton(GLFWwindow *window, int button, int action, int mods);

void MousePosition(GLFWwindow *window, double x, double y);

void MouseScroll(GLFWwindow *window, double x, double y);

void KeyboardButton(GLFWwindow* window, int key, int scancode, int action, int mods);
