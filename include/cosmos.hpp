// sphere.cpp: an easy way to produce detailed Spheres
// @authors Angelos Sfakianakis, Konstantinos Chalas, Alex Shevtsov


#ifndef __COSMOS__
#define __COSMOS__

#include <rendering/camera.hpp>


extern Camera *cam;
extern bool mouse;


void createGalaxy(float x, float y);

void initializeCosmos(void);

void resetCosmos(void);

void changeColor(void);

#endif
