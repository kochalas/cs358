// cosmos.cpp: main file for the Project
// @authors Angelos Sfakianakis, Alex Shevtsov, Konstantinos Chalas


#define FPS_GRANULARITY 100.0f
#define LATENCY         10


#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE


#include <iostream>
#include <chrono>
#include <list>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <AntTweakBar.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <callbacks.hpp>
#include <rendering/camera.hpp>
#include <rendering/texture.hpp>

#include <rendering/background/deep_space.hpp>
#include <rendering/foreground/black_hole.hpp>
#include <rendering/foreground/stars.hpp>

#include <rendering/renderers/black_hole.hpp>
#include <rendering/renderers/stars.hpp>
#include <rendering/renderers/deep_space.hpp>
#include <rendering/renderers/glow.hpp>

#include <cosmos.hpp>


// Scene Properties
static glm::vec3 color = glm::vec3(0.2f, 0.5f, 0.0f);
static glm::vec3 stars_color = glm::vec3(0.15f, 0.45f, 0.5f);
static unsigned int cluster_size = 100;
static unsigned int cluster_count = 50;
static float cluster_mass = 0.002f;
static glm::vec3 bhole_color = glm::vec3(0.45f, 0.8f, 0.9f);
static float bhole_mass = 1.0f;
static glm::vec3 bhole_speed = glm::vec3(0.0f, 0.0f, 0.0f);
static float radius = 3.0f;

// Simulation Properties
static bool wireframe = false;
static bool freeze = false;
static float sim_speed = 1.0f;
bool mouse = true;

// Window & Attributes
static GLFWwindow *window;
static TwBar *tweakBar;
Camera *cam;
static glm::vec3 cam_vec;

// Timekeeping
static auto scene_time = std::chrono::steady_clock::now();
static unsigned int _fps = 0;
static float fps = 0.0f;

// Rendering & Simulation
static const std::list<CelestialBody *> *objects[3]; /* NULL-terminated */
static GlowR *glow;
static BlackHoleR *bhr;
static StarsR *scr;
static DeepSpaceR *dsr;
static DeepSpace *ds;

// Frame Buffer
static GLuint fbo;
static GLuint fb_textures[3];
static const GLuint fb_attachments[] = {
    GL_COLOR_ATTACHMENT0,
    GL_COLOR_ATTACHMENT1
};

// Textures
static Texture *noise;
static Texture *universe;


// simple function to get Time Intervals between calls
static float dt(void) {
    static auto checkpoint = std::chrono::steady_clock::now();
    auto _time = scene_time;
    scene_time = std::chrono::steady_clock::now();

    if (freeze) _time = scene_time;

    float interval = static_cast<std::chrono::duration<float>>(scene_time - _time).count();
    if (++_fps == FPS_GRANULARITY) /* update fps counter */ {
        fps = FPS_GRANULARITY / static_cast<std::chrono::duration<float>>(scene_time - checkpoint).count();
        checkpoint = scene_time;
        _fps = 0; /* reset */
    }

    return interval;
}


static bool initializeWindow(void) {
    static bool isInitialized = false;

    if (isInitialized)
        return true;
    isInitialized = true;

    // initialize GLFW
    if (!glfwInit()) {
        fprintf(stderr, "GLFW initialization failed...\n");
        return false;
    }

    // specify OpenGL version and profile
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 4); /* 4x antialiasing */

    // open a new Window and create its OpenGL context
    GLFWmonitor *monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode *mode = glfwGetVideoMode(monitor);
    if (!(window = glfwCreateWindow(mode->width, mode->height, "Cosmos Simulation", monitor, NULL))) {
        fprintf(stderr, "Unable to create a Window...\n");
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(window);
    glfwSetCursorPos(window, mode->width / 2, mode->height / 2);

    // initialize OpenGL
    glewExperimental = true;
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "GLEW initialization failed...\n");
        return false;
    }
    glViewport(0, 0, mode->width, mode->height);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f); /* background color */

    // enable the Depth Test & Face Culling
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDisable(GL_CULL_FACE);

    return true;
}

static bool initializeHandlers(void) {
    glfwSetInputMode(window, GLFW_STICKY_MOUSE_BUTTONS, GL_TRUE);

    glfwSetMouseButtonCallback(window, MouseButton);
    glfwSetCursorPosCallback(window, MousePosition);
    glfwSetKeyCallback(window,KeyboardButton);
    glfwSetScrollCallback(window, MouseScroll);

    return true;
}

static bool initializeTweakBar(void) {
    // create a new Tweak Bar and attach it to the Window
    const GLFWvidmode *mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    TwInit(TW_OPENGL_CORE, NULL);
    TwWindowSize(mode->width, mode->height);
    tweakBar = TwNewBar("Properties");
    TwDefine("Properties size='225 415'");

    // create the Variables on the Tweak Bar
    TwAddVarRO(tweakBar, "vector", TW_TYPE_DIR3F, &cam_vec, "label='Camera' opened='true'");
    TwAddVarRO(tweakBar, "fps", TW_TYPE_FLOAT, &fps, "label='FPS' group='Simulation' precision=0");
    TwAddVarRW(tweakBar, "wireframe", TW_TYPE_BOOLCPP, &wireframe, "label='Wireframe' key='W' group='Simulation'");
    TwAddVarRW(tweakBar, "freeze", TW_TYPE_BOOLCPP, &freeze, "label='Freeze' key='F' group='Simulation'");
    TwAddVarRW(tweakBar, "mouse", TW_TYPE_BOOLCPP, &mouse, "label='Mouse' group='Simulation'");
    TwAddVarRW(tweakBar, "smulation speed", TW_TYPE_FLOAT, &sim_speed, "label='Speed' group='Simulation' step=0.01 min=0.0 max=10.0");
    TwAddVarRW(tweakBar, "color", TW_TYPE_COLOR3F, &color, "label='Color' group='Environment'");
    TwAddVarRW(tweakBar, "scolor", TW_TYPE_COLOR3F, &stars_color, "label='Star Color' group='Galaxy'");
    TwAddVarRW(tweakBar, "ssize", TW_TYPE_UINT32, &cluster_size, "label='Cluster Size' group='Galaxy' step=1 min=1 max=1000");
    TwAddVarRW(tweakBar, "scount", TW_TYPE_UINT32, &cluster_count, "label='Cluster Count' group='Galaxy' step=1 min=5 max=500");
    TwAddVarRW(tweakBar, "smass", TW_TYPE_FLOAT, &cluster_mass, "label='Cluster Mass' group='Galaxy' step=0.0001 min=0.0001 max=0.05");
    TwAddVarRW(tweakBar, "bcolor", TW_TYPE_COLOR3F, &bhole_color, "label='Black Hole Color' group='Galaxy'");
    TwAddVarRW(tweakBar, "bmass", TW_TYPE_FLOAT, &bhole_mass, "label='Black Hole Mass' group='Galaxy' step=0.1 min=0.2 max=10.0");
    TwAddVarRW(tweakBar, "bspeed", TW_TYPE_DIR3F, &bhole_speed, "label='Black Hole Speed' group='Galaxy'");
    TwAddVarRW(tweakBar, "radius", TW_TYPE_FLOAT, &radius, "label='Radius' group='Galaxy' step=0.1 min=1.0 max=10.0");

    return true;
}


static bool initializeContext(void) {
    const GLFWvidmode *mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

    //initialize the Camera
    cam = new Camera(
            mode->width / (float) mode->height,
            glm::vec3(0.0f, 0.0f, 0.0f),
            glm::vec3(0.0f, 0.0f, -1.0f)
    );

    // load the Textures
    try {
        noise = new Texture("noise.png", GL_TEXTURE_2D);
        universe = new Texture("universe.png", GL_TEXTURE_2D);
    } catch (...) {
        std::cout << "Failed to initialize the Textures...\n";
        return false;
    }

    // initialize the Renderers
    try {
        glow = new GlowR("passthrough.vert", "glow.frag", mode->width, mode->height);
        bhr = new BlackHoleR("move.vert", "horizon.frag");
        scr = new StarsR("sworm.vert", "light.frag", "stars.geom");
        dsr = new DeepSpaceR("transform.vert", "twinkle.frag");
    } catch (...) {
        std::cout << "Failed to initialize the Renderers...\n";
        return false;
    }

    // initialize the Object List
    objects[0] = reinterpret_cast<const std::list<CelestialBody *> *>(bhr->getObjects());
    objects[1] = reinterpret_cast<const std::list<CelestialBody *> *>(scr->getObjects());

    // and a background to be displayed in Deep Space
    ds = new DeepSpace(
            cam->GetCameraVector(),
            glm::vec3(0.5, 0.5f, 0.5f),
            dsr->getProgram()
    );
    dsr->addObject(ds);

    return true;
}

static GLuint generateFramebufferAttachment(GLuint type, int width, int height) {
    GLuint texture;

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, type, width, height, 0, type, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    return texture;
}

static bool initializeFBuffer(void) {
    const GLFWvidmode *mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

    // generate the Textures for the Framebuffer
    fb_textures[0] = generateFramebufferAttachment(GL_DEPTH_COMPONENT, mode->width, mode->height);
    fb_textures[1] = generateFramebufferAttachment(GL_RGBA, mode->width, mode->height);
    fb_textures[2] = generateFramebufferAttachment(GL_RGBA, mode->width, mode->height);

    // generate and bind a Framebuffer
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, fb_textures[0], 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, fb_textures[1], 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, fb_textures[2], 0);

    // verify the Framebuffer
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        return false;

    return true;
}


static void finalizeWindow(void) {
    // destroy the Window and free any OpenGL resources
    glfwTerminate();
}

static void finalizeTweakBar(void) {
    if (tweakBar) TwTerminate();
}

static void finalizeContext(void) {
    // delete the Renderers
    delete glow;
    delete bhr;
    delete scr;
    delete dsr;

    // delete the Textures
    delete noise;
    delete universe;

    //delete camera
    delete cam;
}


static void finalizeFBuffer(void) {
    glDeleteTextures(3, fb_textures);
    glDeleteFramebuffers(1, &fbo);
}


static void display(void) {
    static float time = 0.0f;
    float _time;

    // redirect Rendering to the internal Framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glDrawBuffers(2, fb_attachments);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    if (wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_DEPTH_TEST);

    // refresh the Camera Direction vector
    cam_vec = glm::normalize(cam->GetDirectionVector());

    // calculate the Rendering Attributes
    glm::mat4 View = cam->GetViewMatrix();
    glm::mat4 Projection = cam->GetProjectionMatrix();
    glm::mat4 ViewProjection = Projection * View;

    time += (_time = 0.5f * sim_speed * dt()); /* get time interval */

    // bind the Textures
    noise->bind(GL_TEXTURE0);
    universe->bind(GL_TEXTURE1);

    // render Deep Space
    static unsigned int latency = LATENCY;
    if (latency++ == LATENCY) /* update the background stars */ {
        dsr->setTime(time);
        latency = 0; /* reset */
    }
    dsr->setTransformations(ViewProjection);
    ds->setColor(color);
    dsr->render(_time);

    // render the Black Holes
    bhr->setTransformations(View, Projection);
    bhr->setTime(time);
    bhr->render(_time);

    // render the Star Clusters
    scr->setTransformations(ViewProjection);
    scr->render(_time);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    // apply a Glow Effect and render the Scene
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    texture_bind(GL_TEXTURE0, GL_TEXTURE_2D, fb_textures[1]);
    texture_bind(GL_TEXTURE1, GL_TEXTURE_2D, fb_textures[2]);
    glow->render(0);

    TwDraw();
    glfwSwapBuffers(window);
    glfwPollEvents();
}

void _createGalaxy(float bhole_mass, unsigned int cluster_count, glm::vec3 position) {
    // create the central Black Hole for the Galaxy
    BlackHole *black_hole = new BlackHole(
            position,
            bhole_speed, bhole_mass, bhole_color,
            objects, bhr->getProgram()
    );
    bhr->addObject(black_hole);

    // create Star Clusters around the Black Hole
    float sign = (rand() % 2) ? 1.0f : -1.0f; /* randomize the rotation direction */
    for (unsigned int i = 0; i < cluster_count; i++) {
        // calculate the original Radius and Angle of the Trajectory
        float r = (rand() / (float) RAND_MAX * radius) + (glm::pow(bhole_mass, 1 / 3.0f) / DENSITY);
        float angle = (rand() / (float) RAND_MAX * 2.0f * glm::pi<float>());

        // calculate the Position and Linear Velocity
        glm::vec4 _velocity = glm::rotate(
                glm::mat4(1.0f),
                angle,
                glm::vec3(0.0f, 1.0f, 0.0f)
        ) * glm::vec4(glm::sqrt(bhole_mass / r), 0.0f, 0.0f, 1.0f);
        glm::vec3 stars_pos = position
                + glm::cross(glm::normalize(_velocity.xyz()), glm::vec3(0.0f, 1.0f, 0.0f)) * r;
        glm::vec3 velocity = sign * _velocity.xyz() / _velocity.w + bhole_speed;

        // create new Cluster
        Stars *star_cluster = new Stars(
            stars_pos,
            velocity, cluster_mass, cluster_size,
            stars_color, objects, scr->getProgram()
        );
        scr->addObject(star_cluster);
    }
}

void createGalaxy(float x, float y) {
    std::cout << "mouse = <" << x << ", " << y << ">\n";

    glm::vec3 position = cam->GetCameraVector() + 10.0f * cam->GetDirectionVector();
    _createGalaxy(bhole_mass, cluster_count, position);
}


void initializeCosmos(void) {
    glm::vec3 Positions[4] = {
        glm::vec3(-3.0f, -2.0f, -8.0f),
        glm::vec3(2.0f, 1.f, -5.0f),
        glm::vec3(12.0f, 0.0f, -1.0f),
        glm::vec3(-5.0f, -5.0f, 2.0f)
    };

    resetCosmos();
    for (glm::vec3 p : Positions){
        _createGalaxy((rand() / (float) RAND_MAX) * 2.0f + 1.0f, cluster_count, p);
        changeColor();

    }
}

void resetCosmos(void) {
    finalizeContext();
    initializeContext();
}

void changeColor(void) {
    static int color_iter = 0;

    static glm::vec3 black_hole_colors[7] = {
        glm::vec3(0.25f, 0.0f, 0.75f),
        glm::vec3(0.6f, 0.1f, 0.35f),
        glm::vec3(0.5f, 0.85f, 0.95f),
        glm::vec3(0.7f, 0.5f, 0.2f),
        glm::vec3(0.85f, 0.7f, 0.1f),
        glm::vec3(0.35f, 0.35f, 0.45f),
        glm::vec3(0.15f, 0.8f, 0.45f)
    };
    static glm::vec3 cluster_colors[7] = {
        glm::vec3(0.15f, 0.45f, 0.5f),
        glm::vec3(0.2f, 0.3f, 0.5f),
        glm::vec3(0.2f, 0.55f, 0.45f),
        glm::vec3(1.0f, 0.3f, 0.2f),
        glm::vec3(0.2f, 0.65f, 0.55f),
        glm::vec3(0.4f, 0.0f, 0.4f),
        glm::vec3(0.2f, 0.25f, 0.5f)
    };

    bhole_color = black_hole_colors[color_iter];
    stars_color = cluster_colors[color_iter++];
    if (color_iter > 5) color_iter = 0;
}


int main(void) {
    bool running = true;
    if (initializeWindow() && initializeHandlers() && initializeTweakBar()
            && initializeContext() && initializeFBuffer())
        while (running) {
            display();
            running = !glfwGetKey(window, GLFW_KEY_ESCAPE) && !glfwWindowShouldClose(window);
        }

    finalizeWindow();
    finalizeTweakBar();
    finalizeContext();
    finalizeFBuffer();
}
