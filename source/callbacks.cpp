// callbacks.cpp: callback functions for GLFW
// @authors Alex Shevtsov, Angelos Sfakianakis, Konstantinos Chalas



#define MOUSE_DELTA 0.1f
#define ZOOM_DELTA 0.5f

#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <iostream>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>
#include <AntTweakBar.h>
#include <vector>

#include <cosmos.hpp>

#include <callbacks.hpp>


void MouseButton(GLFWwindow *window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT
            && !TwEventMouseButtonGLFW(button, action)
            && action == GLFW_PRESS) /* send event to AntTweakBar */ {
        double x, y;

        glfwGetCursorPos(window, &x, &y);

        //create new galaxy
        createGalaxy((float) x, (float) y);
    }
}

void MouseScroll(GLFWwindow *window, double x, double y){
    cam->ChangeZoom(-1.0f * ZOOM_DELTA * y);
}

void MousePosition(GLFWwindow *window, double x, double y) {
    if (!TwEventMousePosGLFW(x, y)){
        int width, height;
        glfwGetWindowSize(window, &width, &height);
        float theta = (x - width / 4) / (float) (width / 2) * M_PI; /* angle starts from z, goes right */
        float phi = M_PI + (y - height / 2) / (float) height * M_PI; /* angle starts from z, goes up */

       // calculate the Direction using Spherical Coordinates
        cam->ProcessMouseMovement(glm::vec3(
                cos(theta) * cos(phi),
                sin(phi),
                sin(theta) * cos(phi)
        ));
    }
}

void KeyboardButton(GLFWwindow* window, int key, int scancode, int action, int mods) {
    // remap the Special Keys for AntTweakBar
    int _key = key;
    if (key == GLFW_KEY_ENTER)
        _key = 294;
    else if (key == GLFW_KEY_BACKSPACE)
        _key = 295;
    else if (key == GLFW_KEY_DELETE)
        _key = 297;

    // handle the Keyboard Input
    if (key != GLFW_KEY_ESCAPE && !TwEventKeyGLFW(_key, action) && !TwEventCharGLFW(key, action)
            && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
        switch(key){
        case GLFW_KEY_UP: /* forward */
            cam->ProcessKeyboard(cam->GetCameraVector() +
                            MOUSE_DELTA * cam->GetDirectionVector());
            break;
        case GLFW_KEY_DOWN: /* backward */
            cam->ProcessKeyboard(cam->GetCameraVector() -
                            MOUSE_DELTA * cam->GetDirectionVector());
            break;
        case GLFW_KEY_LEFT: /* left */
            cam->ProcessKeyboard(cam->GetCameraVector() +
                        MOUSE_DELTA * glm::normalize(
                        glm::cross(glm::vec3(0.0f, 1.0f, 0.0f),
                        cam->GetDirectionVector() ) ) );
            break;
        case GLFW_KEY_RIGHT: /* right */
            cam->ProcessKeyboard(cam->GetCameraVector() +
                        MOUSE_DELTA * glm::normalize(
            glm::cross(cam->GetDirectionVector(),
                    glm::vec3(0.0f, 1.0f, 0.0f) ) ) );
            break;
        case GLFW_KEY_M:
            mouse = !mouse;
            cam->ChangeMouseMove();
            break;
        case GLFW_KEY_R:
            resetCosmos();
            break;
        case GLFW_KEY_I:
            initializeCosmos();
            break;
        case GLFW_KEY_C:
            changeColor();
            break;
        }
    }
}
