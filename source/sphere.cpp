// sphere.cpp: an easy way to produce detailed Spheres
// @authors Angelos Sfakianakis, Konstantinos Chalas, Alex Shevtsov


#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <unordered_map>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

#include <sphere.hpp>
#include <GL/glext.h>


// a factory that generates new Spheres
SphereFactory factory(SPHERE_RESOLUTION);


void SphereFactory::initialize(void) {;
    num_points = 12;
    num_triangles = 20;
    points = new glm::vec3[num_points]; /* allocate memory for the vertices */
    triangles = new Triangle[num_triangles]; /* allocate memory for the triangles */

    float phi = (1.0f + sqrt(5.0f)) / 2.0f; /* golden ratio */

    // calculate the 12 basic Vertices of the Icosahedron
    points[0] = glm::vec3(-1.0f,  phi,  0.0f);
    points[1] = glm::vec3(1.0f,  phi,  0.0f);
    points[2] = glm::vec3(-1.0f,  -phi,  0.0f);
    points[3] = glm::vec3(1.0f,  -phi,  0.0f);
    points[4] = glm::vec3(0.0f, -1.0f,  phi);
    points[5] = glm::vec3(0.0f, 1.0f,  phi);
    points[6] = glm::vec3(0.0f, -1.0f,  -phi);
    points[7] = glm::vec3(0.0f, 1.0f,  -phi);
    points[8] = glm::vec3(phi,  0.0f, -1.0f);
    points[9] = glm::vec3(phi,  0.0f, 1.0f);
    points[10] = glm::vec3(-phi,  0.0f, -1.0f);
    points[11] = glm::vec3(-phi,  0.0f, 1.0f);

    // now create the Triangular Faces from the Vertices
    //create ring of basic circle
    Triangle::prepare(triangles[0], 5, 0, 11);
    Triangle::prepare(triangles[1], 4, 5, 11);
    Triangle::prepare(triangles[2], 9, 5, 4);
    Triangle::prepare(triangles[3], 3, 9, 4);
    Triangle::prepare(triangles[4], 8, 9, 3);
    Triangle::prepare(triangles[5], 6, 8, 3);
    Triangle::prepare(triangles[6], 7, 8, 6);
    Triangle::prepare(triangles[7], 10, 7, 6);
    Triangle::prepare(triangles[8], 0, 7, 10);
    Triangle::prepare(triangles[9], 11, 0, 10);
    //create first cone of basic circle
    Triangle::prepare(triangles[10], 5, 1, 0);
    Triangle::prepare(triangles[11], 9, 1, 5);
    Triangle::prepare(triangles[12], 8, 1, 9);
    Triangle::prepare(triangles[13], 7, 1, 8);
    Triangle::prepare(triangles[14], 0, 1, 7);
    //create second cone of basic circle
    Triangle::prepare(triangles[15], 4, 11, 2);
    Triangle::prepare(triangles[16], 3, 4, 2);
    Triangle::prepare(triangles[17], 6, 3, 2);
    Triangle::prepare(triangles[18], 10, 6, 2);
    Triangle::prepare(triangles[19], 11, 10, 2);
}

GLuint SphereFactory::fetch_point(std::unordered_map<unsigned int, unsigned int> &dictionary,
                      unsigned int a, unsigned int b) {
    unsigned int key = (a > b) ? (a << 16) | b : (b << 16) | a;

    try {
        return dictionary.at(key);
    } catch (std::out_of_range &ex) {
        points[num_points] = (points[a] + points[b]) / 2.0f;
        dictionary.insert(std::make_pair(key, num_points));
        return num_points++;
    }
}

void SphereFactory::refine(void) {
    std::unordered_map<unsigned int, unsigned int> dictionary;

    glm::vec3 *points_old = points;
    points = new glm::vec3[6 * num_triangles];
    for (unsigned int i = 0; i < num_points; i++)
        points[i] = points_old[i];

    Triangle *triangles_old = triangles;
    unsigned int num_trianglesO = num_triangles;
    num_triangles *= 4;
    triangles = new Triangle[num_triangles];

    unsigned int j = 0; /* pointer in the new triangles array */
    for (unsigned int i = 0; i < num_trianglesO; i++) /* split each triangle into four */ {
        Triangle triangle = triangles_old[i];

        Triangle::prepare(
                triangles[j++],
                triangle.a,
                fetch_point(dictionary, triangle.a, triangle.b),
                fetch_point(dictionary, triangle.a, triangle.c)
        ); /* generate top sub-triangle */
        Triangle::prepare(
                triangles[j++],
                triangle.b,
                fetch_point(dictionary, triangle.b, triangle.c),
                fetch_point(dictionary, triangle.a, triangle.b)
        ); /* generate left sub-triangle */
        Triangle::prepare(
                triangles[j++],
                triangle.c,
                fetch_point(dictionary, triangle.a, triangle.c),
                fetch_point(dictionary, triangle.b, triangle.c)
        ); /* generate right sub-triangle */
        Triangle::prepare(
                triangles[j++],
                fetch_point(dictionary, triangle.a, triangle.b),
                fetch_point(dictionary, triangle.b, triangle.c),
                fetch_point(dictionary, triangle.c, triangle.a)
        ); /* generate middle sub-triangle */
    }
    assert(j == num_triangles);

    delete[] points_old;
    delete[] triangles_old;
}


void SphereFactory::inflate(float radius) {
    for (unsigned int i = 0; i < num_points; i++) /* process all points */
        points[i] = radius * glm::normalize(points[i]);
}


/**
 * Create a new Sphere Factory. This is used to generate multiple Spheres,
 * without recalculating all the Points and Normals.
 *
 * @param resolution The number of recursive calls to the refining function.
 */
SphereFactory::SphereFactory(unsigned int resolution) {
    initialize(); /* create the original icosahedron */
    for (unsigned int i = 0; i < resolution; i++) /* refine recursively */
        refine();

    /* we do not need to calculate the Normals, because each point is also
    perpendicular to the Sphere's surface, thus - if normalized - each point
    can represent its own Normal */
}


unsigned int SphereFactory::create(float radius, GLuint &indices, GLuint &points) {
    inflate(radius); /* inflate the sphere */

    // send the points to the GPU
    glGenBuffers(1, &points);
    glBindBuffer(GL_ARRAY_BUFFER, points);
    glBufferData(GL_ARRAY_BUFFER, num_points * sizeof(glm::vec3), this->points, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    // send the indices to the GPU
    glGenBuffers(1, &indices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, num_triangles * 3 * sizeof(GLuint), this->triangles, GL_STATIC_DRAW);

    return 3 * num_triangles;
}
