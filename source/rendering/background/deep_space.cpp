// deep_space.cpp: drawable object used as the background of the cosmos
// @author Angelos Sfakianakis, Alex Shevtsov, Konstantinos Chalas


#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <cassert>
#include <iostream>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <sphere.hpp>

#include <rendering/background/deep_space.hpp>


DeepSpace::DeepSpace(glm::vec3 position, glm::vec3 color, GLuint program)
    : position(position), color(color) {
    // send the Sphere to the GPU
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    count = factory.create(SPACE_RADIUS, indices, points);
    glBindVertexArray(0);

    // query the Program for the Uniform Locations
    GLint texture0, texture1;
    gl_color = glGetUniformLocation(program, "color");
    texture0 = glGetUniformLocation(program, "noise");
    texture1 = glGetUniformLocation(program, "universe");
    if (gl_color < 0 || texture0 < 0 || texture1 < 0)
        throw -1;

    // specify the Texture Units to use
    glUseProgram(program);
    glUniform1i(texture0, 0);
    glUniform1i(texture1, 1);
    glUseProgram(0);
}


void DeepSpace::draw(void) const {
    // send the Uniform to the GPU
    glUniform3fv(gl_color, 1, glm::value_ptr(color));

    // bind the Vertex Array Object and draw
    glBindVertexArray(vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices);
    glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, NULL);
    glBindVertexArray(0);
}

bool DeepSpace::update(float dt) {
    return true;
};
