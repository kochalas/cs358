// camera.cpp: a simple moving Camera
// @authors Alex Shevtsov, Angelos Sfakianakis, Konstantinos Chalas


#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <iostream>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <rendering/camera.hpp>


// Constructor with vectors
Camera::Camera(float aspect_ratio, glm::vec3 position, glm::vec3 direction)
        : position(position), direction(direction), aspectRatio(aspect_ratio) {
    this->should_move = true;
    this->updateCameraVectors();
    this->zoom = 45.0f;
}


glm::mat4 Camera::GetViewMatrix() {
    return view;
}

glm::mat4 Camera::GetProjectionMatrix() {
    return glm::perspective(glm::radians(this->zoom), this->aspectRatio, NEAR_CLIP, FAR_CLIP);
}

void Camera::ProcessKeyboard(glm::vec3 camera) {
    if (glm::length(camera) < 50.0f) /* limit the camera to a specific radius */ {
        this->position = camera;
        this->updateCameraVectors();
    }
}

void Camera::ProcessMouseMovement(glm::vec3 direction) {
    if (this->should_move){
        this->direction = direction;
        this->updateCameraVectors();
    }
}

void Camera::updateCameraVectors() {
    // create an Eye Matrix
    view = glm::lookAt(
            this->position, /* camera location */
            this->direction + this->position, /* look at the direction of the mouse (spherical) */
            glm::vec3(0.0f, 1.0f, 0.0f) /* camera is in upright position */
    );

   // generate the final View-Projection Matrix
    eye = this->GetProjectionMatrix() * view; /* WCS -> ECS -> CSS */
}
