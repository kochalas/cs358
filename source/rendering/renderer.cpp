// renderer.hpp: renders a batch of Drawables using the same OpenGL program
// @authors Alex Shevtsov, Angelos Sfakianakis, Konstantinos Chalas


#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cassert>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <rendering/renderer.hpp>


/**
 * Reads a Shader from file.
 *
 * @param file The path of the file to read (relative to the shaders' directory).
 *
 * @return The code in the Shader file.
 */
std::string shader_read(const std::string &file) throw(std::ifstream::failure) {
    assert(!file.empty());

    std::ifstream input;
    std::stringstream string;

    // read the Shader code into Memory as text
    input.open(SHADERS_DIRECTORY + file);
    input.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    string << input.rdbuf();
    input.close();

    return string.str();
}

/**
 * Compiles a Shader.
 *
 * @param code The code of the SHader to compile.
 * @param type The type of the Shader (i.e GL_VERTEX_SHADER,
 * GL_FRAGMENT_SHADER, GL_GEOMETRY_SHADER)
 *
 * @return A handle to the newly compiled Shader.
 */
GLuint shader_compile(const std::string &code, GLenum type) {
    GLuint shader;
    GLint success;

    // create a new Shader and compile the Code
    const GLchar *code_string = code.c_str();
    shader = glCreateShader(type);
    glShaderSource(shader, 1, &code_string, NULL);
    glCompileShader(shader);

    // check Shader compilation status
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) /* report errors & throw exception */ {
        GLchar log[LOG_SIZE];
        GLsizei log_length;

        glGetShaderInfoLog(shader, LOG_SIZE, &log_length, log);
        std::cout << log << std:: endl;

        throw -1;
    }

    return shader;
}


Renderer::Renderer(const std::string &vertex_shader,
                   const std::string &fragment_shader,
                   const std::string &geometry_shader) {
    assert(!vertex_shader.empty() && !fragment_shader.empty());

    program = glCreateProgram();

    // compile the Shaders and link them to the Program
    glAttachShader(program, shader_compile(shader_read(vertex_shader), GL_VERTEX_SHADER));
    glAttachShader(program, shader_compile(shader_read(fragment_shader), GL_FRAGMENT_SHADER));
    if (!geometry_shader.empty())
        glAttachShader(program, shader_compile(shader_read(geometry_shader), GL_GEOMETRY_SHADER));
    glLinkProgram(program);

    // check final Program status
    GLint success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) /* report errors & throw exception */ {
        GLchar log[LOG_SIZE];
        GLsizei log_length;

        glGetProgramInfoLog(program, LOG_SIZE, &log_length, log);
        std::cout << log << std::endl;

        throw -1;
    }
}


void Renderer::render(float dt) {
    glUseProgram(program);

    // update and render all the Drawables
    for (auto drawable = drawables.begin(); drawable != drawables.end(); )
        if ((*drawable)->update(dt) == false) /* delete object */ {
            delete *drawable;
            drawable = drawables.erase(drawable);
        } else /* update & render the object */ {
            (*drawable)->draw();
            drawable++; /* next object */
        }

    glUseProgram(0);
}
