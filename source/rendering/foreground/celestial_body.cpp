// celestial_body.cpp: represents any possible Celestial Body in the scene
// @authors Konstantinos Chalas, Alex Shevtsov, Angelos Sfakianakis


#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <cassert>

#include <glm/glm.hpp>

#include <rendering/foreground/celestial_body.hpp>


bool CelestialBody::update(float dt) {
    assert(objects);

    glm::vec3 acceleration = glm::vec3(0.0f);

    if (this->mass == 0.0f) /* object has expired */
        return false;

    // calculate the Celestial Body's acceleration using the Law of Universal Gravitation
    for (unsigned int i = 0; objects[i] != NULL; i++) /* iterate over all lists */ {
        auto iterator = objects[i]->begin();
        auto guard = objects[i]->end();

        while (iterator != guard) /* iterate over objects */ {
            assert(dynamic_cast<CelestialBody *>((*iterator)));
            CelestialBody *object = static_cast<CelestialBody *>(*iterator);

            if (object != this && object->mass >= 0.0f) /* ignore the object itself */ {
                glm::vec3 distance = object->position - position;
                float distanceL = glm::length(distance);

                if (distanceL <= THRESHOLD_PROXIMITY) /* absorb */
                    absorbMass(*object);
                else /* calculate acceleration due to that object */
                    acceleration += object->mass / glm::pow(glm::length(distance), 3.0f) * distance;
            }

            iterator++; /* next object */
        }
    }

    // update the Celestial Body's velocity and position
    setVelocity(velocity + acceleration * dt);
    setPosition(position + velocity * dt);

    if (distance() >= THRESHOLD_DISTANCE) /* out of bounds */ {
#ifndef NDEBUG
        this->mass = 0.0f; /* object has expired */
#endif
        return false;
    } else return true;
}
