// stars.cpp: drawable object representing a Cluster of Stars
// @author Angelos Sfakianakis, Alex Shevtsov, Konstantinos Chalas


#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <cassert>
#include <cstdlib>
#include <ctime>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <rendering/foreground/stars.hpp>
#include <glm/gtc/type_ptr.hpp>


void Stars::createStarPositions(void) {
    glm::vec3 position;

    for (unsigned int i = 0; i < cluster_size; i++) {
        do {
            position.x = -1 + rand() / ((float) (RAND_MAX / 2));
            position.y = -1 + rand() / ((float) (RAND_MAX / 2));
            position.z = -1 + rand() / ((float) (RAND_MAX / 2));
        } while (glm::length(position) > rand() / ((float) RAND_MAX));

        star_positions[i] = position;
    }
}

Stars::Stars(glm::vec3 position, glm::vec3 velocity, float mass, unsigned int cluster_size, glm::vec3 color,
             const std::list<CelestialBody *> *objects[], GLuint program)
        : CelestialBody(position, velocity, mass, objects), cluster_size(cluster_size), color(color) {
    assert(mass > 0.0f);

    star_positions = (glm::vec3 *) malloc(cluster_size * sizeof(glm::vec3));
    createStarPositions();

    // initialize the History to the current Position
    for (int i = 0; i < PREV_POSITIONS; i++)
        cluster_history[i] = position;

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);

    // send the Positions to the GPU
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, cluster_size * sizeof(glm::vec3), star_positions, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glBindVertexArray(0);

    // query the Program for the Uniform Locations
    gl_position_history = glGetUniformLocation(program, "position_history");
    gl_color = glGetUniformLocation(program, "color");
    GLint texture = glGetUniformLocation(program, "noise");
    if (gl_position_history < 0 || gl_color < 0 || texture < 0)
        throw -1;

    // specify the Texture Units to use
    glUseProgram(program);
    glUniform1i(texture, 0);
    glUseProgram(0);
}


void Stars::draw(void) const {
    // send the Uniforms to the GPU
    glUniform3fv(gl_position_history, PREV_POSITIONS, (GLfloat *) cluster_history);
    glUniform3fv(gl_color, 1, glm::value_ptr(color));

    // bind the Vertex Array Object and draw
    glBindVertexArray(vao);
    glDrawArrays(GL_POINTS, 0, cluster_size);
    glBindVertexArray(0);
}

bool Stars::update(float dt) {
    if (!CelestialBody::update(dt)) /* object has expired */
        return false;

    // shift Previous Positions to the Right and add new
    for(int i = PREV_POSITIONS - 1; i >= 1; i--)
        cluster_history[i] = cluster_history[i - 1];
    cluster_history[0] = position;

    return true;
}
