// black_hole.cpp: drawable object representing a Black Hole
// @author Angelos Sfakianakis, Alex Shevtsov, Konstantinos Chalas


#define GLM_FORCE_INLINE
#define GLM_FORCE_RADIANS


#include <cassert>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <sphere.hpp>

#include <rendering/foreground/black_hole.hpp>
#include <glm/gtc/matrix_transform.hpp>


BlackHole::BlackHole(glm::vec3 position, glm::vec3 velocity, float mass, glm::vec3 color,
          const std::list<CelestialBody *> *objects[], GLuint program)
        : CelestialBody(position, velocity, mass, objects), color(color) {
    assert(mass > 0.0f);

    // send the Sphere to the GPU
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    count = factory.create(1.0f, indices, points);
    glBindVertexArray(0);

    // query the Program for the Uniform Locations
    gl_translate = glGetUniformLocation(program, "translate");
    gl_color = glGetUniformLocation(program, "color");
    gl_radius = glGetUniformLocation(program, "radius");
    GLint texture = glGetUniformLocation(program, "noise");
    if (gl_translate < 0 || gl_color < 0 || gl_radius < 0 || texture < 0)
        throw -1;

    // specify the Texture Units to use
    glUseProgram(program);
    glUniform1i(texture, 0);
    glUseProgram(0);
}

void BlackHole::draw(void) const {
    // send the Uniforms to the GPU
    glUniform3fv(gl_translate, 1, glm::value_ptr(position));
    glUniform3fv(gl_color, 1, glm::value_ptr(color));
    glUniform1f(gl_radius, glm::pow(CelestialBody::getMass(), 1 / 3.0f) / DENSITY);

    // bind the Vertex Array Object and draw
    glBindVertexArray(vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices);
    glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, NULL);
    glBindVertexArray(0);
}
