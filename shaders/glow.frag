#version 330 core


#define AREA 5.0


uniform sampler2D original;
uniform sampler2D highlights;

uniform float resolutionX, resolutionY;

in vec2 fCoordinates;
out vec4 fColor;


vec4 getPixel(vec2 position) {
    // fetch a Pixel and calculate its Alpha Channel (black to alpha)
    vec4 pixel = texture(highlights, position);
    pixel.a = (pixel.r + pixel.g + pixel.b) / 3.0;

    return pixel;
}

void main() {
    // apply Blur to the Highlights
    vec4 highlight = vec4(0.0);
    for (float i = -AREA / resolutionX; i <= AREA / resolutionX; i += 1.0 / resolutionX)
        highlight += getPixel(fCoordinates + vec2(i, 0));
    for (float i = -AREA / resolutionY; i <= AREA / resolutionY; i += 1.0 / resolutionY)
        highlight += getPixel(fCoordinates + vec2(0, i));
    highlight /= 2.0 * AREA;

    // blend the Scene with the Highlights
    fColor = vec4(texture(original, fCoordinates).rgb + highlight.a * highlight.rgb, 1.0);
}
