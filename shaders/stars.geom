#version 330 core


#define STAR_WIDTH  0.01
#define STAR_HEIGHT 0.02

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;
in vec2 coordinates [];
out vec2 f_coordinates;


void main() {
    vec4 position = gl_in[0].gl_Position;
    f_coordinates = coordinates[0];

    // create a Diamond shape for the Star
    gl_Position = position + vec4(0.0f,  -STAR_HEIGHT, 0.0f, 0.0f); /* 1: bottom */
    EmitVertex();
    gl_Position = position + vec4(-STAR_WIDTH,  0.0f, 0.0f, 0.0f); /* 2: left */
    EmitVertex();
    gl_Position = position + vec4(STAR_WIDTH,  0.0f, 0.0f, 0.0f); /* 3: right */
    EmitVertex();
    gl_Position = position + vec4(0.0f, STAR_HEIGHT, 0.0f, 0.0f); /* 4: top */
    EmitVertex();
    EndPrimitive();
}
