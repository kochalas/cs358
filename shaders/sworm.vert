#version 330 core


#define PREV_POSITIONS 30


uniform mat4 ViewProjection;
uniform vec3 position_history[PREV_POSITIONS];

layout (location = 0) in vec3 position;
out vec2 coordinates;


void main() {
    vec3 new_position = vec3(0.0);
    float point = -1.0, weights = 0.0;
    float weight, distance;

    // calculate the Relative Position of each Start within the Cluster
    for (int i = 0; i < PREV_POSITIONS; i++) {
        distance = position.x - point; /* distance from sector center */

        if (distance <= (5.0 / (PREV_POSITIONS - 1))) /* include sector */ {
            weight = 1 / distance; /* inverse distance */
            new_position += weight * position_history[i];
            weights += weight; /* total weight */
        }

        point += 2.0 / (PREV_POSITIONS - 1.0); /* next mass */
    }
    new_position /= weights; /* normalize */

    // calculate the final Position of the Star
    gl_Position = ViewProjection * vec4(position + new_position, 1.0);

    // get the relative Coordinates for the Noise
    coordinates = position.xy;
}
