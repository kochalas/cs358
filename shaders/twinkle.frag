#version 330 core


#define PI 3.141592

#define STANDARD    0.5
#define JITTER      0.50
#define FREQUENCY   1000.0
#define THRESHOLD   0.0001
#define PROBABILITY 0.997


uniform sampler2D noise;
uniform sampler2D universe;
uniform float time;

uniform vec3 color;

in vec3 fPosition;
layout (location = 0) out vec4 fColor;
layout (location = 1) out vec4 gColor;


float _random(float x) {
    return STANDARD + sin(FREQUENCY * time * x) * JITTER;
}

float _sine_sample(float x) {
    return STANDARD + sin(FREQUENCY * x) * JITTER;
}

float sine_sample(vec2 coordinates) {
    vec3 value = texture(noise, coordinates).rgb;
    return (_sine_sample(value.r) + _sine_sample(value.g) + _sine_sample(value.b)) / 3;
}

void main() {
    // calculate the Spherical Coordinates for attaching the Texture
    vec2 coordinates;
    vec3 position = normalize(fPosition); /* inflate sphere (radius=1) */
    coordinates.x = atan(position.z, position.x) / (2.0 * PI) + 0.5;
    coordinates.y = asin(position.y) / PI + 0.5;

    // attach the Universe to the containing Sphere & generate Stars
    fColor = vec4(texture(universe, coordinates).rgb, 1.0);
    if (_sine_sample(texture(noise, coordinates).r) <= THRESHOLD) /* get a candidate star */
        if (_random(texture(noise, coordinates).r) < PROBABILITY) /* draw a star */
            fColor += vec4(color * sine_sample(coordinates), 0.0);
    gColor = vec4(0.0, 0.0, 0.0, 1.0); /* null output */
}
