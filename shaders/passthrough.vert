#version 330 core


layout(location = 0) in vec2 position;
layout(location = 1) in vec2 coordinates;

out vec2 fCoordinates;


void main() {
    fCoordinates = coordinates;
    gl_Position = vec4(position, -1.0, 1.0);
}
