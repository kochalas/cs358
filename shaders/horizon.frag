#version 330 core


#define JITTER      0.35
#define FREQUENCY   1000.0

#define SHININESS   5.0
#define INTENSITY   0.25

#define BRIGHTNESS  1.25
#define THRESHOLD   0.2


uniform sampler2D noise;
uniform float time;
uniform vec3 color;

in vec3 fPosition, fNormal, fView;
in vec2 coordinates;
layout (location = 0) out vec4 fColor;
layout (location = 1) out vec4 gColor;


float _random(float x) {
    return 1.0 + sin(FREQUENCY * time * x) * JITTER;
}

float random() {
    vec3 value = texture(noise, coordinates).rgb;
    return (_random(value.r) + _random(value.g) + _random(value.b)) / 3;
}

void main() {
    // calculate the Glow Intensity
    float intensity = 1 - pow(max(dot(fNormal, fView), 0.0), 2); /* glow from the edges */
    intensity = pow(intensity, SHININESS);

    // apply the Glow Effect with Jitter
    fColor = intensity * random() * vec4(color, 1.0);

    // generate an artificial Equator
    fColor += vec4(pow(1.0 - abs(fPosition.y), SHININESS) * INTENSITY * color, 1.0);

    // output to Color Buffer
    if ((fColor.r + fColor.g + fColor.b) / 3.0 >= THRESHOLD) /* output highlight */
        gColor = BRIGHTNESS * fColor;
    else /* muffle fragment */
        gColor = vec4(0.0, 0.0, 0.0, 1.0);
}
