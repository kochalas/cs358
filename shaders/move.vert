#version 330 core


uniform mat4 View, Projection;
uniform vec3 translate;
uniform float radius;

layout(location = 0) in vec3 vPosition;
out vec3 fPosition, fNormal, fView;
out vec2 coordinates;


void main() {
    // calculate the Normal of the Point (it is a Sphere)
    fNormal = normalize((transpose(inverse(View)) * vec4(vPosition, 1.0)).xyz);

    // calculate the Position of the Point in Eyespace
    vec3 positionT = radius * vPosition + translate;
    vec4 position = View * vec4(positionT, 1.0);

    // calculate the View Vector for the Point
    fView = normalize(-position.xyz);

    // project the Point & send its original Position to the Fragment Shader
    gl_Position = Projection * position;
    fPosition = vPosition;

    // get the relative Coordinates for the Noise
    coordinates = gl_Position.xy;
    coordinates.x = abs(coordinates.x);
    coordinates.y = abs(coordinates.y);
}
