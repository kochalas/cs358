#version 330 core


uniform mat4 ModelViewProjection;

layout(location = 0) in vec3 vPosition;
out vec3 fPosition;


void main() {
    gl_Position = ModelViewProjection * vec4(vPosition, 1.0);
    fPosition = vPosition;
}
