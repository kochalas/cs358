#version 330 core


#define JITTER      0.50
#define FREQUENCY   1000.0

#define BRIGHTNESS  2.0


uniform sampler2D noise;
uniform vec3 color;

in vec2 f_coordinates;
layout (location = 0) out vec4 fColor;
layout (location = 1) out vec4 gColor;


float random(float x) {
    return 1.0 + sin(FREQUENCY * x) * JITTER;
}

void main() {
    // get a sample from the Texture
    vec3 value = texture(noise, f_coordinates).rgb;

    // calculate each Channel individually
    fColor.r = random(value.r) * color.r;
    fColor.g = random(value.g) * color.g;
    fColor.b = random(value.b) * color.b;
    fColor.w = 1.0f;

    gColor = BRIGHTNESS * fColor;
}
